CO=prezentace
# usage: make filename.pdf
#%.pdf: %.tex
#	pdflatex $<
#	pdflatex $<
all: build

.PHONY: build
build:
	latexmk -pdf -shell-escape prezentace.tex

clean:
	rm -f *.dvi *.log $(CO).blg $(CO).bbl $(CO).toc *.aux $(CO).out $(CO).lof $(CO).fls $(CO).fdb_latexmk $(CO).nav $(CO).snm
	rm -f $(CO).pdf
	rm -f $(CO).rubbercache
	rm -f $(CO).synctex.gz
	rm -f *~
